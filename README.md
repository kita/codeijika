# Codeijika

Codeijika is a Discord bot that uses the Forgejo (and maybe even Gitea) APIs to retrieve specific information on demand.

And if you're wondering, yes, she's named after the character Nijika Ijichi from Bocchi The Rock.

# Setup

1. Clone this repository:

```
git clone https://codeberg.org/kita/codeijika
```

2. Create a virtual environment and activate it:

```
python -m venv venv
source venv/bin/activate
```

3. Install the needed dependencies:

```
pip install discord.py pyforgejo
```

4. Create a file named `config.json` in the root of the folder, then fill in this content:

```json
{
    "instanceApiUrl": "https://<replace with preferred instance>/api/v1",
    "token": "<your discord bot token>",
    "forgejoApiToken": "<your forgejo api token>"
}
```

Don't worry, we will teach you how to to get a Discord bot token and Forgejo API token.

5. Create a personal access token. It is recommended to use an entirely seperate account for this purpose.

* First, click on your profile picture at the top right, then go to Settings

![](/repometa/userSettings.png)

* Then go to Applications. You will see something like this. Ignore the existing tokens.

![](/repometa/forgejoApplications.png)

* Then go to the "Create new token" section and fill them in EXACTLY like this. The name can be anything you want, but name it Codeijika to easily identify it:

![](/repometa/applicationScope.png)

* Click on "Generate Token", and you will be given a token that will go away once you leave the page, so copy it now and put it on the configuration file.

![](/repometa/forgejoApiToken.png)

6. Create a Discord bot

> Note: you must have a Discord account in order to do this.

* First, go to https://discord.com/developers/applications, and you will be taken to a page like this:

![](/repometa/discordDeveloper.png)

* Click on "New Application", and put in any name you want.

![](/repometa/newApp.png)

* You will now be taken to a page like this:

![](/repometa/appSettings.png)

* Then go to Bot, then click on Reset Token, so it will now give you a token. If it already shows your token, copy it and put it on the configuration file.

![](/repometa/resetToken.png)

![](/repometa/discordToken.png)

*And no the token isn't valid :3*

7. After all of that involved work, you can now start the bot:

```
(venv) kita@starry:~/codeijika $ python run.py
[ 07-10-2024 17:23:51    W/discord.client            ]: PyNaCl is not installed, voice will NOT be supported
[ 07-10-2024 17:23:51    W/discord.ext.commands.bot  ]: Privileged message content intent is missing, commands may not work as expected.
[ 07-10-2024 17:23:51    I/discord.client            ]: logging in using static token
[ 07-10-2024 17:23:52    I/nijika                    ]: Checking connectivity to Forgejo API
[ 07-10-2024 17:23:53    I/httpx                     ]: HTTP Request: GET https://codeberg.org/api/v1/version "HTTP/1.1 200 OK"
[ 07-10-2024 17:23:53    I/nijika                    ]: Forgejo version: 7.0.0-368-09be816934+gitea-1.21.11
[ 07-10-2024 17:23:54    I/discord.gateway           ]: Shard ID None has connected to Gateway (Session ID: baf804b7fa72272a56c8284b5954d20d).
[ 07-10-2024 17:23:57    I/httpx                     ]: HTTP Request: GET https://codeberg.org/api/v1/user/settings "HTTP/1.1 200 OK"
[ 07-10-2024 17:23:57    I/httpx                     ]: HTTP Request: GET https://codeberg.org/api/v1/user "HTTP/1.1 200 OK"
[ 07-10-2024 17:23:57    I/nijika                    ]: Bot bot#1234 (123456789012345) is up and running!
[ 07-10-2024 17:23:57    I/nijika                    ]: Invite the bot to your server: https://discord.com/oauth2/authorize?client_id=123456789012345&scope=bot+applications.commands&permissions=51200
```

You can now invite the bot to servers that you manage or have permissions to using the link the bot prints out in the console.

# License

Copyright &copy; 2024 Ikuyo Kita

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.