from pyforgejo import AuthenticatedClient
from pyforgejo.api.miscellaneous import get_version
from pyforgejo.api.user import get_user_settings, user_get_current

import discord
from discord.ext import commands
import json
import logging
import os
from typing import TypedDict

from errors import Forbidden, NotFound


class BotConfiguration(TypedDict):
    instanceApiUrl: str
    token: str
    forgejoApiToken: str
    noreplyEmail: str


log = logging.getLogger("nijika")
cogs = ("cogs.core", "cogs.user", "cogs.org", "cogs.repo")

class Nijika(commands.Bot):
    """Nijika my beloved <3"""

    def __init__(self):

        self.configuration: BotConfiguration = json.load(open("config.json"))
        self.forgejoClient: AuthenticatedClient = AuthenticatedClient(
                base_url=self.configuration["instanceApiUrl"],
#                raise_on_unexpected_status=True,
                follow_redirects=True,
                token=self.configuration["forgejoApiToken"],
                httpx_args={'event_hooks': {'response': [self.handleErrorCodes]}})

        self.accent_color = 0xecd97a
        self.error_color = 0xbb3955

        self.userConfig: dict = None
        self.currentUser: dict = None

        super().__init__(
                command_prefix="=",  # unused
                intents=discord.Intents.default(),
                allowed_mentions=discord.AllowedMentions(
                    everyone=False, users=False, roles=False))

    async def handleErrorCodes(self, res):
        if res.status_code == 403:
            raise Forbidden()
        if res.status_code == 404:
            raise NotFound()

    async def checkConnectivity(self):
        """Checks connectivity to Instance's Forgejo API"""
        log.info("Checking connectivity to Forgejo API")
        resp = await get_version.asyncio_detailed(client=self.forgejoClient)
        log.info("Forgejo version: %s", json.loads(resp.content)["version"])

    async def setup_hook(self):
        await self.checkConnectivity()

    async def on_ready(self):
        for cog in cogs:
            try:
                await self.load_extension(cog)
            except Exception:
                log.exception("Cannot load cog: %s", cog)

        if not os.path.exists(".initDone"):
            log.info("Initializing first run hooks.")
            # this bot fully uses slash commands. sync them on
            # first run
            await self.tree.sync()

            open(".initDone", "w").write("")
            log.info("Done.")

        self.userConfig = json.loads(
                (await get_user_settings.asyncio_detailed(client=self.forgejoClient)).content
            )

        self.currentUser = json.loads(
                (await user_get_current.asyncio_detailed(client=self.forgejoClient)).content
            )

        log.info("Bot %s (%d) is up and running!", self.user, self.user.id)
        log.info("Invite the bot to your server: %s",
                 discord.utils.oauth_url(self.application_id,
                                         permissions=discord.Permissions(
                                             send_messages=True,
                                             embed_links=True,
                                             attach_files=True
                                             )))

    async def start(self):
        """Start up Codeijika"""
        await super().start(self.configuration["token"])

    async def stop(self):
        """Stop Codeijika"""
        await self.close()
