from __future__ import annotations

from pyforgejo.api.user import user_get_current

import discord
from discord import app_commands
from discord.ext import commands

from datetime import datetime
import json
import logging
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from bot import Nijika

log = logging.getLogger("nijika")


class Core(commands.Cog):

    def __init__(self, bot: Nijika):
        self.bot: Nijika = bot

    async def interaction_check(self, i: discord.Interaction):

        if not await self.bot.is_owner(i.user):
            await i.response.send_message(embed=discord.Embed(
                title="Permission denied",
                description="You are not the bot owner.",
                color=self.bot.error_color
                ))
            return False
        else:
            return True

    cogGroup = app_commands.Group(name="cog", description="Cog handling")

    @cogGroup.command(name='load', description="Load a cog (owner only)")
    @app_commands.describe(cog="Cog to load")
    async def _cogLoad(self, i: discord.Interaction, cog: str):
        try:
            await self.bot.load_extension(cog)
        except Exception as e:
            log.exception(
                    "Exception occurred trying to load %s",
                    cog, exc_info=e)
            return await i.response.send_message(embed=discord.Embed(
                title="Cog has failed to load. See console for details",
                description=f"{e.__class__.__name__}: {e}",
                color=self.bot.error_color
                ), ephemeral=True)
        return await i.response.send_message("Done!", ephemeral=True)

    @cogGroup.command(name='unload', description="Unload a cog (owner only)")
    @app_commands.describe(cog="Cog to unload")
    async def _cogUnload(self, i: discord.Interaction, cog: str):
        try:
            if cog == "cogs.core":
                return await i.response.send_message(embed=discord.Embed(
                    title="Error",
                    description="Are you fucking stupid?",
                    color=self.bot.error_color
                    ))
            await self.bot.remove_extension(cog)
        except Exception as e:
            log.exception(
                    "Exception occurred trying to unload %s",
                    cog, exc_info=e)
            return await i.response.send_message(embed=discord.Embed(
                title="Cog has failed to unload. See console for details",
                description=f"{e.__class__.__name__}: {e}",
                color=self.bot.error_color
                ), ephemeral=True)
        return await i.response.send_message("Done!", ephemeral=True)

    @cogGroup.command(name='reload', description="Reload a cog (owner only)")
    @app_commands.describe(cog="Cog to reload")
    async def _cogReload(self, i: discord.Interaction, cog: str):
        try:
            await self.bot.reload_extension(cog)
        except Exception as e:
            log.exception(
                    "Exception occurred trying to reload %s",
                    cog, exc_info=e)
            return await i.response.send_message(embed=discord.Embed(
                title="Cog has failed to reload. See console for details",
                description=f"{e.__class__.__name__}: {e}",
                color=self.bot.error_color
                ), ephemeral=True)
        return await i.response.send_message("Done!", ephemeral=True)

    @app_commands.command(name="sync", description="Sync command tree")
    @app_commands.describe(_global="Whether to sync globally",
                           copy="Whether to copy global commands to guild",
                           guild="Guild ID to sync to, leave blank for current context")
    async def _sync(self, i: discord.Interaction, _global: bool = False,
                    copy: bool = False, guild: int = None):

        if guild:
            guild = discord.Object(guild)
        else:
            guild = i.guild

        if _global:
            cmds = await self.bot.tree.sync()
            return await i.response.send_message(f"Synced {len(cmds)} commands.", ephemeral=True)

        if guild:
            if copy:
                await self.bot.tree.copy_global_to(guild=guild)
            cmds = await self.bot.tree.sync(guild=guild)
            return await i.response.send_message(f"Synced {len(commands)} commands.", ephemeral=True)
        else:
            return await i.response.send_message(embed=discord.Embed(
                title="Error",
                description="Specify a guild.",
                color=self.bot.error_color
                ), ephemeral=True)

    @app_commands.command(name="whoami", description="Get current authenticated user (Owner Only)")
    async def _whoami(self, i: discord.Interaction):

        req = await user_get_current.asyncio_detailed(client=self.bot.forgejoClient)
        data = json.loads(req.content)

        return await i.response.send_message(embed=discord.Embed(
            title="Authenticated User Information",
            description=(
                "# Basic information\n"
                "**Username:** {username}\n"
                "**Full Name:** {fullName}\n"
                "**Location:** {location}\n"
                "**Email:** {email}\n"
                "**Is Administrator:** {isAdmin}\n"
                "\n"
                "# Statistics\n"
                "**Join Date:** {joinedAt}\n"
                "**Followers:** {followers}\n"
                "**Following:** {following}\n"
                "**Starred Repositories:** {starredRepos}"
                ).format(
                    username=data["username"],
                    fullName=data["full_name"] if data["full_name"] != "" else "None",
                    location=data["location"] if data["location"] != "" else "None",
                    email=data["email"] if data["email"] != "" else "None/Hidden",
                    isAdmin=data["is_admin"],
                    joinedAt=datetime.fromisoformat(data["created"]),
                    followers=data["followers_count"],
                    following=data["following_count"],
                    starredRepos=data["starred_repos_count"]
                    ),
            color=self.bot.accent_color
            ).set_thumbnail(url=data["avatar_url"]))

async def setup(bot: Nijika):
    await bot.add_cog(Core(bot))
