from __future__ import annotations

from pyforgejo.api.organization import org_get, org_list_members

import discord
from discord import app_commands
from discord.ext import commands

from errors import Forbidden, NotFound

import json
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from bot import Nijika

class Org(commands.Cog):

    def __init__(self, bot: Nijika):
        self.bot: Nijika = bot

    grp = app_commands.Group(name="org", description="Organization related commands.")

    @grp.command(name="get", description="Get an organization")
    @app_commands.describe(org="Organization to get")
    async def _org_get(self, i: discord.Interaction, org: str):

        await i.response.defer()

        try:
            org_get_payload = await org_get.asyncio_detailed(
                    org, client=self.bot.forgejoClient)

            data = json.loads(org_get_payload.content)
        except NotFound:
            return await i.followup.send(embed=discord.Embed(
                description="I could not find that organization.",
                color=self.bot.error_color
                ))

        try:
            org_members_payload = await org_list_members.asyncio_detailed(
                    org, client=self.bot.forgejoClient)

            memberCount = org_members_payload.headers["x-total-count"]
        except Forbidden:
            memberCount = "Inaccessible"

        await i.followup.send(embed=discord.Embed(
            title="Organization information",
            description=(
                "**ID**: {_id}\n"
                "**Name**: {name}\n"
                "**Full Name**: {fullName}\n"
                "**Email**: {email}\n"
                "**Website:** {website}\n"
                "**Location:** {location}\n"
                "**Repository admin can change access for teams**: {teamAccess}\n"
                "**Visible members count**: {memberCount}\n"
                "## Description\n"
                "{description}"
                ).format(
                    _id=data["id"],
                    name=data["name"],
                    fullName=data["full_name"] if data["full_name"] != "" else "None",
                    email=data["email"] if data["email"] != "" else "None",
                    website=data["website"] if data["website"] != "" else "None",
                    location=data["location"] if data["location"] != "" else "None",
                    teamAccess=data["repo_admin_change_team_access"],
                    memberCount=memberCount,
                    description=data["description"] if data["description"] != "" else "None"
                    ),
                color=self.bot.accent_color
            ).set_thumbnail(url=data["avatar_url"]))

async def setup(bot: Nijika):
    await bot.add_cog(Org(bot))
