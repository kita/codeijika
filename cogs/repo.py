from __future__ import annotations

from pyforgejo import AuthenticatedClient
from pyforgejo.api.repository import (
        repo_get,
        repo_list_topics,
        repo_search
)

import discord
from discord import app_commands
from discord.ext import commands
from discord import ui

from errors import NotFound
from paginator import ButtonPaginator
import utils

from datetime import datetime
import logging
import json
from typing import TYPE_CHECKING

log = logging.getLogger("nijika.repo")

if TYPE_CHECKING:
    from bot import Nijika

class RepoPages(ButtonPaginator):

    def __init__(self, query, color, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.query = query
        self.color=color

    def format_page(self, page):
        embed = discord.Embed(
            title = "Search Results" if not self.query else f"Results For {self.query}",
            description="\n".join(
                [f"[{x['full_name']}]({x['html_url']}) ({x['id']})" for x in page]
                ),
            color=self.color
        )
        embed.set_footer(text=f"Page {self.current_page + 1}/{self.max_pages}, Total: {len(self.pages)}")
        return embed

class Repo(commands.Cog):

    def __init__(self, bot: Nijika):
        self.bot: Nijika = bot

    grp = app_commands.Group(
            name="repo",
            description="Repository related commands"
            )

    @grp.command(name="get", description="Get a repository")
    @app_commands.describe(
            target=("Target repository. "
                    "Example: kita/codeijika"))
    async def repoGet(self, i: discord.Interaction, target: str):

        await i.response.defer()

        targetFmt = target.split("/")
        if not len(targetFmt) == 2:
            return await i.followup.send(embed=discord.Embed(
                title="Invalid format",
                color=self.bot.error_color
                ))
        try:
            res = await repo_get.asyncio_detailed(
                    targetFmt[0],
                    targetFmt[1],
                    client=self.bot.forgejoClient
                    )
        except NotFound:
            return await i.followup.send(embed=discord.Embed(
                title="Error",
                description=("Could not find repository. "
                             "Please make sure the repository exists and is "
                             "not private."),
                color=self.bot.error_color
                ))

        topicRes = await repo_list_topics.asyncio_detailed(
                targetFmt[0],
                targetFmt[1],
                client=self.bot.forgejoClient
                )

        data = json.loads(res.content)
        topicData = json.loads(topicRes.content)

        if data["empty"]:
            return await i.followup.send(
                "The repository you specified does not contain any content.")

        embed = discord.Embed(
             title=data["full_name"],
             url=data["html_url"],
             color=self.bot.accent_color
        )

        if data["avatar_url"] != "":
            embed.set_thumbnail(url=data["avatar_url"])

        embed.set_footer(text=(
            "Created: {created} • "
            "Last updated: {last}").format(
                created=datetime.fromisoformat(data["created_at"]),
                last=datetime.fromisoformat(data["updated_at"])
                ))

        embed.set_author(
                name=data["owner"]["username"],
                url=utils.build_instance_url(
                    data["owner"]["username"]
                    ),
                icon_url=data["owner"]["avatar_url"]
                )

        if data["language"] != "":
            embed.add_field(
                name="Programming Language", value=data["language"],
                inline=False)

        embed.add_field(
                name="Stars", value=data["stars_count"]) \
             .add_field(
                name="Forks", value=data["forks_count"]) \
             .add_field(
                name="Watchers", value=data["watchers_count"])

        if data["has_issues"]:
            embed.add_field(
                name="Issues", value=data["open_issues_count"])

        if data["has_pull_requests"]:
            embed.add_field(
                name="PRs", value=data["open_pr_counter"])

        if len(topicData["topics"]) > 0:
            embed.add_field(
                name="Topics", value=", ".join(topicData["topics"]),
                inline=False)

        return await i.followup.send(embed=embed)

    @grp.command(name="search", description="Search for repositories")
    @app_commands.describe(
        q="Search Query",
        topic="Search query is a topic",
        include_desc="Match results from descriptions as well",
        uid="Search only for repos that the user with the given ID owns or contributes to",
        priority_owner_id="Repo owner ID to prioritize in results",
        starred_by="Search for repos that the user with the given ID has starred",
        template="Include templates (defaults to true)",
        exclusive="If uid is specified, only search for repositories the user with the given ID owns",
        archived="Whether to include archived repositories as well",
        sort="Sort by (defaults to Alpha)",
        mode="Type of repository to search.",
        order="Sorting order"
        )
    @app_commands.choices(
        sort=[
            app_commands.Choice(name="Alpha", value="alpha"),
            app_commands.Choice(name="Created", value="created"),
            app_commands.Choice(name="Updated", value="updated"),
            app_commands.Choice(name="Size", value="size"),
            app_commands.Choice(name="Git Size", value="git_size"),
            app_commands.Choice(name="Git LFS Size", value="lfs_size"),
            app_commands.Choice(name="Stars", value="stars"),
            app_commands.Choice(name="Forks", value="forks"),
            app_commands.Choice(name="ID", value="id")
        ],
        order=[
            app_commands.Choice(name="Ascending", value="asc"),
            app_commands.Choice(name="Descending", value="desc")
        ],
        mode=[
            app_commands.Choice(name="Fork", value="fork"),
            app_commands.Choice(name="Source", value="source"),
            app_commands.Choice(name="Mirror", value="mirror"),
            app_commands.Choice(name="Collaborative", value="collaborative")
        ]
    )
    async def repoSearch(self, i: discord.Interaction, q: str = None,
                         topic: bool = None, include_desc: bool = None,
                         uid: int = None, priority_owner_id: int = None,
                         starred_by: int = None, template: bool = None,
                         exclusive: bool = None, archived: bool = None,
                         mode: str = None, sort: str = None,
                         order: str = None):

        await i.response.defer()

        res = await repo_search.asyncio_detailed(
            client=self.bot.forgejoClient,
            q=q,
            topic=topic,
            include_desc=include_desc,
            uid=uid,
            priority_owner_id=priority_owner_id,
            starred_by=starred_by,
            template=template,
            exclusive=exclusive,
            archived=archived,
            mode=mode,
            sort=sort,
            order=order,
            limit=250)

        data = json.loads(res.content)

        if len(data["data"]) == 0:

            return await i.followup.send(embed=discord.Embed(
                description="No results found.",
                color=self.bot.error_color
            ))

        page = RepoPages(q, self.bot.accent_color, data["data"], author_id=i.user.id, per_page=20)
        await page.start(i)



async def setup(bot: Nijika):
    await bot.add_cog(Repo(bot))
