from __future__ import annotations

from pyforgejo.api.user import user_get

import discord
from discord import app_commands
from discord.ext import commands

from errors import NotFound

from datetime import datetime
import json
from typing import TYPE_CHECKING
from utils import (
        SingularLinkButton,
        check_for_noreply, build_instance_url)

if TYPE_CHECKING:
    from bot import Nijika
class User(commands.Cog):

    def __init__(self, bot: Nijika):
        self.bot: Nijika = bot
    userGroup = app_commands.Group(name="user", description="User related commands")

    @userGroup.command(name="get", description="Get a user")
    @app_commands.describe(user="User to get")
    async def getUser(self, i: discord.Interaction, user: str):

        await i.response.defer()

        try:
            res = await user_get.asyncio_detailed(user, client=self.bot.forgejoClient)
        except NotFound:
            return await i.followup.send(embed=discord.Embed(
                title="I couldn't find that user.",
                color=self.bot.error_color
                ))
        data = json.loads(res.content)

        # If the user was to somehow know the associated user
        # for the bot and hiding email address is enabled, hide it

        if user == self.bot.currentUser['username'] and self.bot.userConfig['hide_email']:
            email = "Hidden"
        else:
            email = data["email"] if not check_for_noreply(data["email"]) else "Hidden"

        return await i.followup.send(embed=discord.Embed(
            description=(
                "# Basic information\n"
                "**User ID:** {userId}\n"
                "**Username:** {username}\n"
                "**Full Name:** {fullName}\n"
                "**Location:** {location}\n"
                "**Email:** {email}\n"
                "\n"
                "# Statistics\n"
                "**Join Date:** {joinedAt}\n"
                "**Followers:** {followers}\n"
                "**Following:** {following}\n"
                "**Starred Repositories:** {starredRepos}"
                ).format(
                    userId=data["id"],
                    username=data["username"],
                    fullName=data["full_name"] if data["full_name"] != "" else "None",
                    location=data["location"] if data["location"] != "" else "None",
                    email=email,
                    joinedAt=datetime.fromisoformat(data["created"]),
                    followers=data["followers_count"],
                    following=data["following_count"],
                    starredRepos=data["starred_repos_count"]
                    ),
            color=self.bot.accent_color
            ).set_thumbnail(url=data["avatar_url"]),
            view=SingularLinkButton(
                label="Visit Profile",
                url=build_instance_url(data["username"])
            ))

    @userGroup.command(
            name="avatar",
            description="Get a user/organization's avatar")
    @app_commands.describe(
            target="Target user/organization"
            )
    async def userAvatar(self, i: discord.Interaction, target: str):

        await i.response.defer()

        try:
            res = await user_get.asyncio_detailed(
                    target,
                    client=self.bot.forgejoClient)
        
        except NotFound:
            return i.followup.send(
                    title="I could not find the user/organization.",
                    color=self.bot.error_color
                    )
        
        data = json.loads(res.content)

        return await i.followup.send(
            embed=discord.Embed(
                title="{}'s avatar".format(
                    data["full_name"] if data["full_name"] != ""
                    else data["username"]
                ),
                color=self.bot.accent_color
            )
            .set_image(url=data["avatar_url"])
            .set_author(
                name=data["username"],
                url=build_instance_url(data["username"]),
                icon_url=data["avatar_url"]
                ))

async def setup(bot: Nijika):
    await bot.add_cog(User(bot))

