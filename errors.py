class ForgejoException(Exception):
    pass

class Forbidden(ForgejoException):
    """Raised when Forgejo API returns a 403"""
    pass

class NotFound(ForgejoException):
    """Raised when data on the Forgejo API isn't found."""
    pass
