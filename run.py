from bot import Nijika
import discord
import logging
import contextlib
from logging.handlers import RotatingFileHandler
import asyncio

@contextlib.contextmanager
def setup_logging():
    log = logging.getLogger()
    dateFmt = "%m-%d-%Y %H:%M:%S"
    discord.utils.setup_logging(formatter=logging.Formatter("[ {asctime}    {levelname:.1}/{name:<25} ]: {message}", dateFmt, style="{"))

    try:
        logging.getLogger("discord").setLevel(logging.INFO)
        logging.getLogger("discord.http").setLevel(logging.WARNING)

        handler = RotatingFileHandler(filename='codeijika.log', encoding='utf-8', mode='w', maxBytes=32*1024*1024, backupCount=5)
        fmt = logging.Formatter('[{asctime}] [{levelname:<7}] {name}: {message}', dateFmt, style='{')
        handler.setFormatter(fmt)
        log.addHandler(handler)
        yield
    finally:
        handlers = log.handlers[:]
        for hdlr in handlers:
            hdlr.close()
            log.removeHandler(hdlr)

async def main():
    async with Nijika() as bot:
        await bot.start()

if __name__ == "__main__":
    with setup_logging():
        asyncio.run(main())
