import json
from functools import cache

import discord
from discord import ui

config = json.load(open("config.json"))


class SingularLinkButton(ui.View):

    def __init__(self, *, label: str, url: str,
                 style: discord.ButtonStyle = discord.ButtonStyle.gray):
        super().__init__()

        self.add_item(ui.Button(
            style=style,
            label=label,
            url=url
            ))


@cache
def get_domain() -> str:
    """Returns the domain"""

    return str(config["instanceApiUrl"]).split("/")[2]


def build_instance_url(x) -> str:
    """Simply prepends the instance url
    (example: https://codeberg.org) with x"""

    proto = str(config["instanceApiUrl"]).split("/")[0]
    return proto + "//" + get_domain() + "/" + x


def check_for_noreply(email: str) -> bool:
    """
    Checks if the email is a "no reply" email.
    If the email is a no reply email, it returns True,
    otherwise returns False.

    If the noreplyEmail value is set, it will check
    that value first, falling back to the api domain
    (example: if the domain is kitachan.rocks then it will be
    nijika@noreply.kitachan.rocks) if it does not exist.
    """
    if "noreplyEmail" in config:
        if config["noreplyEmail"] in email:
            return True
    else:
        # assume noreply email is "noreply.domain.org" (example: "nijika@noreply.kitachan.rocks")

        domain = get_domain()
        noreply = "noreply." + domain

        if noreply in email:
            return True
    return False
